I compiled freemind 1.1.0 beta 2 in eclipse to use in my linux desktop.

To use it, 


cd ~

git clone git@gitlab.com:qwertyjune/freemind.git

mv ~/freemind ~/.freemind


To run in terminal,

$HOME/.freemind/dist/freemind.sh


To create a shortcut in applications menu,

mkdir -p ~/.local/share/applications

cp ~/.freemind/freemind.desktop ~/.local/share/applications/
